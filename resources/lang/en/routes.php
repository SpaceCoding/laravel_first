<?php
return [
    "users"    =>  "All users",
    "about"  =>  "About page",
    'en' => 'English',
    'ru' => 'Russian',
    'uz' => 'Ўзбек',
    'uz-Latn' => 'O‘zbek',
];