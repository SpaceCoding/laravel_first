<?php
return [
    "users"    =>  "Все пользователи",
    "about"  =>  "О сайте",
    'en' => 'English',
    'ru' => 'Russian',
    'uz' => 'Ўзбек',
    'uz-Latn' => 'O‘zbek',
];