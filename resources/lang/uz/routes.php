<?php
return [
    "users"    =>  "Барча фойдаланувчилар",
    "about"  =>  "Сайт ҳақида",
    'en' => 'English',
    'ru' => 'Russian',
    'uz' => 'Ўзбек',
    'uz-Latn' => 'O‘zbek',
];