<?php
return [
    "users"    =>  "Barcha foydalanuvchilar",
    "about"  =>  "Sayt haqida",
    'en' => 'English',
    'ru' => 'Russian',
    'uz' => 'Ўзбек',
    'uz-Latn' => 'O‘zbek',
];